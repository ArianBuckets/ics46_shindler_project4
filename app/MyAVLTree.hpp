#ifndef __PROJ_FOUR_AVL_HPP
#define __PROJ_FOUR_AVL_HPP

#include "runtimeexcept.hpp"
#include <string>
#include <vector>
#include <memory>
#include <iostream>

class ElementNotFoundException : public RuntimeException 
{
public:
	ElementNotFoundException(const std::string & err) : RuntimeException(err) {}
};


template<typename Key, typename Value>
class MyAVLTree
{
private:
	// fill in private member data here
	// If you need to declare private functions, do so here too.

	struct Node
	{
		Key key;
		Value value;
		int height = 0;
		Node* left = nullptr;
		Node* right = nullptr;
		Node* parent = nullptr;
		// std::unique_ptr<Node> left;
		// std::unique_ptr<Node> right;
	};
	unsigned int sz;
	Node* root;
	// std::unique_ptr<Noderoot;

public:
	MyAVLTree();

	// In general, a copy constructor and assignment operator
	// are good things to have.
	// For this quarter, I am not requiring these. 
	//	MyAVLTree(const MyAVLTree & st);
	//	MyAVLTree & operator=(const MyAVLTree & st);


	// The destructor is, however, required. 
	~MyAVLTree();

	// size() returns the number of distinct keys in the tree.
	size_t size() const noexcept;

	// isEmpty() returns true if and only if the tree has no values in it. 
	bool isEmpty() const noexcept;

	// contains() returns true if and only if there
	//  is a (key, value) pair in the tree
	//	that has the given key as its key.
	bool contains(const Key & k) const; 

	// find returns the value associated with the given key
	// If !contains(k), this will throw an ElementNotFoundException
	// There needs to be a version for const and non-const MyAVLTrees.
	Value & find(const Key & k);
	const Value & find(const Key & k) const;

	// Inserts the given key-value pair into 
	// the tree and performs the AVL re-balance
	// operation, as described in lecture. 
	// If the key already exists in the tree, 
	// you may do as you please (no test cases in
	// the grading script will deal with this situation)
	void insert(const Key & k, const Value & v);


	// function to rebalance, consists of calls to subfunctions.
	void Rebalance(Node* tree, Node* current_node);

	void UpdateHeight(Node* current_node);

	int GetBalance(Node* current_node);

	void RotateLeft(Node* tree,  Node* current_node);

	void RotateRight(Node* tree,  Node* current_node);

	// BOOL or VOID?????????????????????????????????????????????????????
	void ReplaceChild(Node* parent, Node* current_child, Node* new_child);


	void SetChild(Node* parent, std::string which_child, Node* child);

	// Necessary functions needed to rebalance. Source Zybooks


	// in general, a "remove" function would be here
	// It's a little trickier with an AVL tree
	// and I am not requiring it for this quarter's ICS 46.
	// You should still read about the remove operation
	// in your textbook. 

	// The following three functions all return
	// the set of keys in the tree as a standard vector.
	// Each returns them in a different order.
	void RecurInOrder(std::vector<Key> & the_vec, Node* cur_node) const;
	void RecurPreOrder(std::vector<Key> & the_vec, Node* cur_node) const;
	void RecurPostOrder(std::vector<Key> & the_vec, Node* cur_node) const;
	std::vector<Key> inOrder() const;
	std::vector<Key> preOrder() const;
	std::vector<Key> postOrder() const;

	void FindAndIncrement(const Key & k);

	void DestroyRecursive(Node* current_node);


};


template<typename Key, typename Value>
MyAVLTree<Key,Value>::MyAVLTree()
	: sz{0}, root{nullptr}
{

}


template<typename Key, typename Value>
MyAVLTree<Key,Value>::~MyAVLTree()
{
	this->DestroyRecursive(root);
	
}


template<typename Key, typename Value>
void MyAVLTree<Key,Value>::DestroyRecursive(Node* current_node)
{
	if (current_node == nullptr)
	{
		return;
	}
	this->DestroyRecursive(current_node->left);
	this->DestroyRecursive(current_node->right);
	delete current_node;
}


template<typename Key, typename Value>
size_t MyAVLTree<Key, Value>::size() const noexcept
{
	return sz;
}


template<typename Key, typename Value>
bool MyAVLTree<Key, Value>::isEmpty() const noexcept
{
	if (sz == 0) 
	{
		return true;
	}
	else
	{
		return false;
	}
	
}


template<typename Key, typename Value>
bool MyAVLTree<Key, Value>::contains(const Key &k) const
{
	Node* current_node = root;
	while (current_node != nullptr)
	{
		if (current_node->key == k)
		{
			return true;
		}
		else
		{
			if (k > current_node->key)
			{
				current_node = current_node->right;
			}
			else
			{
				current_node = current_node->left;
			}
			
		}
		
	}
	
	return false; // stub
}


template<typename Key, typename Value>
Value & MyAVLTree<Key, Value>::find(const Key & k)
{
	Node* current_node = root;

	while (current_node != nullptr)
	{
		if (k == current_node->key)
		{
			// std::cout << current_node->value << " --   " << std::endl;
			return current_node->value;
		}
		else if (k < current_node->key)
		{
			current_node = current_node->left;
		}
		else
		{
			current_node = current_node->right;
		}
	}
	throw ElementNotFoundException("Not found...");	
}


template<typename Key, typename Value>
const Value & MyAVLTree<Key, Value>::find(const Key & k) const
{
	Node* current_node = root;

	while (current_node != nullptr)
	{
		if (k == current_node->key)
		{
			return current_node->value;
		}
		else if (k < current_node->key)
		{
			current_node = current_node->left;
		}
		else
		{
			current_node = current_node->right;
		}
	}
	throw ElementNotFoundException("Not found...");	
}


template<typename Key, typename Value>
void MyAVLTree<Key,Value>::FindAndIncrement(const Key & k)
{
	Node* current_node = root;

	while (current_node != nullptr)
	{
		if (k == current_node->key)
		{
			current_node->value +=1;
			break;
		}
		else if (k < current_node->key)
		{
			current_node = current_node->left;
		}
		else
		{
			current_node = current_node->right;
		}
	}
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::UpdateHeight(Node* current_node)
{
	int left_height = -1;
	if (current_node->left != nullptr)
	{
		left_height = current_node->left->height;
	}
	int right_height = -1;
	if (current_node->right != nullptr)
	{
		right_height = current_node->right->height;
	}

	if (left_height >= right_height)
	{
		current_node->height = left_height + 1;
	}
	if (right_height > left_height)
	{
		current_node->height = right_height + 1;
	}
}


template<typename Key, typename Value>
int MyAVLTree<Key, Value>::GetBalance(Node* current_node)
{
	int left_height = -1;
	if (current_node->left != nullptr)
	{
		left_height = current_node->left->height;
	}
	int right_height = -1;
	if (current_node->right != nullptr)
	{
		right_height = current_node->right->height;
	}
	return left_height - right_height;
}




template<typename Key, typename Value>
void MyAVLTree<Key, Value>::ReplaceChild(Node* parent, Node* current_child, Node* new_child)
{
	if (parent->left == current_child)
	{
		this->SetChild(parent, "left", new_child);
	}
	else if (parent->right == current_child)
	{
		this->SetChild(parent, "right", new_child);
	}
	
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::SetChild(Node* parent, std::string which_child, Node* child)
{
	if (which_child != "left" && which_child != "right")
	{
		return;
	}
	if (which_child == "left")
	{
		parent->left = child;
	}
	if (which_child == "right")
	{
		parent->right = child;
	}
	if (child != nullptr)
	{
		child->parent = parent;
	}
	
	this->UpdateHeight(parent);
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::RotateRight(Node* tree,  Node* current_node)
{
	Node* left_right_child = current_node->left->right;
	if(current_node->parent != nullptr)
	{
		this->ReplaceChild(current_node->parent, current_node, current_node->left);
	}
	else
	{
		this->root = current_node->left;
		this->root->parent = nullptr;
	}
	
	this->SetChild(current_node->left, "right", current_node);
	this->SetChild(current_node, "left", left_right_child);
	// return;
}

template<typename Key, typename Value>
void MyAVLTree<Key, Value>::RotateLeft(Node* tree,  Node* current_node)
{
	Node* left_right_child = current_node->right->left;
	if(current_node->parent != nullptr)
	{
		this->ReplaceChild(current_node->parent, current_node, current_node->right);
	}
	else
	{
		this->root = current_node->right;
		this->root->parent = nullptr;
	}
	
	this->SetChild(current_node->right, "left", current_node);
	this->SetChild(current_node, "right", left_right_child);
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::Rebalance(Node* tree, Node* current_node)
{
	
	this->UpdateHeight(current_node);
	if (this->GetBalance(current_node) == -2)
	{
		if(this->GetBalance(current_node->right) == 1)
		{
			this->RotateRight(tree, current_node->right);
		}
		this->RotateLeft(tree, current_node);
		
	}
	else if (this->GetBalance(current_node) == 2)
	{
		if (this->GetBalance(current_node->left) == -1)
		{
			this->RotateLeft(tree, current_node->left);
		}
		this->RotateRight(tree, current_node);
	}
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::insert(const Key & k, const Value & v)
{
	Node* new_node = new Node;
	new_node->key = k;
	new_node->value = v;
	new_node->left = nullptr;
	new_node->right = nullptr;
	new_node->parent = nullptr;


	if (this->contains(k) == false)
	{	
		if (this->isEmpty())
		{
			this->root = new_node;
			new_node->parent = nullptr;
			++sz;
		}
		else
		{
			Node* current_node = root;
			while (current_node != nullptr)
			{
				if (k < current_node->key)
				{
					if (current_node->left == nullptr)
					{
						current_node->left = new_node;
						new_node->parent = current_node;
						current_node = nullptr;
						++sz;
					}
					else
					{
						current_node = current_node->left;
					}
				}
				else
				{
					if (current_node->right == nullptr)
					{
						current_node->right = new_node;
						new_node->parent = current_node;
						current_node = nullptr;
						++sz;
					}
					else
					{
						current_node = current_node->right;
					}
				}
				
			}
		}
	}
	else
	{
		this->FindAndIncrement(k);
	}
	
	
	
	
	// rebalance 
	new_node = new_node->parent;
	while (new_node != nullptr)
	{
		this->Rebalance(this->root,new_node);
		new_node = new_node->parent;
	}
	
}


template<typename Key, typename Value>
std::vector<Key> MyAVLTree<Key, Value>::inOrder() const
{
	std::vector<Key> in_order;
	Node* current_node = root;
	MyAVLTree<Key,Value>::RecurInOrder(in_order, current_node);
	return in_order;
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::RecurInOrder(std::vector<Key> & the_vec, Node* cur_node) const
{
	if (cur_node == nullptr)
	{
		return;
	}
	RecurInOrder(the_vec,cur_node->left);
	the_vec.push_back(cur_node->key);
	RecurInOrder(the_vec,cur_node->right);
	


}

// template<typename Key, typename Value>
// std::vector<Key> MyAVLTree<Key, Value>::preOrder() const
// {
// 	std::vector<Key> foo;
// 	return foo; 
// }


template<typename Key, typename Value>
std::vector<Key> MyAVLTree<Key, Value>::preOrder() const
{
	std::vector<Key> pre_order;
	Node* current_node = root;
	MyAVLTree<Key,Value>::RecurPreOrder(pre_order, current_node);
	return pre_order;
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::RecurPreOrder(std::vector<Key> & the_vec, Node* cur_node) const
{
	if (cur_node == nullptr)
	{
		return;
	}
	the_vec.push_back(cur_node->key);
	RecurPreOrder(the_vec,cur_node->left);
	RecurPreOrder(the_vec,cur_node->right);
}


// template<typename Key, typename Value>
// std::vector<Key> MyAVLTree<Key, Value>::postOrder() const
// {
// 	std::vector<Key> foo;
// 	return foo; 
// }


template<typename Key, typename Value>
std::vector<Key> MyAVLTree<Key, Value>::postOrder() const
{
	std::vector<Key> post_order;
	Node* current_node = root;
	MyAVLTree<Key,Value>::RecurPostOrder(post_order, current_node);
	return post_order;
}


template<typename Key, typename Value>
void MyAVLTree<Key, Value>::RecurPostOrder(std::vector<Key> & the_vec, Node* cur_node) const
{
	if (cur_node == nullptr)
	{
		return;
	}
	RecurPostOrder(the_vec,cur_node->left);
	RecurPostOrder(the_vec,cur_node->right);
	the_vec.push_back(cur_node->key);
	
}




#endif 