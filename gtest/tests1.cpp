#include "gtest/gtest.h"
#include "MyAVLTree.hpp"
#include "proj4.hpp"
#include <string>
#include <sstream>
#include <vector>

#include <iostream>
#include <fstream>


namespace{


// NOTE:  these are not intended as exhaustive tests.
// This should get you started testing.

// The four tests marked "CheckPoint" are going to be run
// on your submission for the checkpoint code.
// This is worth one-sixth of your grade on the assignment
// and is due at the time marked "checkpoint"


// None of the "checkpoint" tests require you to have
// AVL functionality OR the counting of words.
// Implementing your tree as a plain binary search
// tree is more than enough to pass these tests.

// Of course, you are expected to implement AVL functionality
// for the full project.

// BE SURE TO FULLY TEST YOUR CODE.
// This means making sure your templating is not hard-coding for 
// a specific type, that every function is called somewhere in 
// your test cases, etc. 
// There will NOT be a project 2 style redo for this;  if your 
//  code does not compile, your score will be a zero. 


TEST(CheckPoint, CheckPoint_FindTheRoot)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(5, "foo");

	EXPECT_TRUE( tree.contains(5) );
}

TEST(CheckPoint, CheckPoint_FindOneHop)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(5, "foo");
	tree.insert(10, "bar");

	EXPECT_TRUE( tree.contains(10) );
}

TEST(CheckPoint, CheckPoint_FindTwoHops)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(5, "foo");
	tree.insert(3, "sna");
	tree.insert(10, "bar");
	tree.insert(12, "twelve");

	EXPECT_TRUE( tree.contains(12) );
}



TEST(CheckPoint, CheckPoint_Add5)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(5, "foo");
	tree.insert(3, "sna");
	tree.insert(10, "bar");
	tree.insert(12, "twelve");
	tree.insert(15, "fifteen");

	EXPECT_TRUE( tree.size() == 5 );
}

TEST(MyTests, String_keys)
{
	MyAVLTree<std::string, std::string> tree;
	tree.insert("Boo", "14");
	tree.insert("Alex", "45");
	tree.insert("Arian", "21");
	tree.insert("Eric", "eleven");

	EXPECT_TRUE( tree.size() == 4 );
}

TEST(MyTests, FindStringKeys)
{
	MyAVLTree<std::string, std::string> tree;
	tree.insert("Boo", "14");
	tree.insert("Alex", "45");
	tree.insert("Arian", "21");
	tree.insert("Eric", "eleven");

	std::string exp = "eleven";
	EXPECT_TRUE(exp == tree.find("Eric"));
}

TEST(MyTests, FindIntKeys)
{
	MyAVLTree<int, int> tree;
	tree.insert(5, 2005);
	tree.insert(3, 2003);
	tree.insert(10, 2010);
	tree.insert(12, 2012);
	tree.insert(15, 2015);

	int exp = 2010;
	EXPECT_TRUE(exp == tree.find(10));
}

TEST(MyTests, FindThrowsException)
{
	MyAVLTree<int, int> tree;
	tree.insert(5, 2005);
	tree.insert(3, 2003);
	tree.insert(10, 2010);
	tree.insert(12, 2012);
	tree.insert(15, 2015);

	EXPECT_ANY_THROW(tree.find(69));
	
}

TEST(MyTests, InOrder1)
{
	MyAVLTree<int,std::string> tree;
	tree.insert(5, "foo");
	tree.insert(3, "sna");
	tree.insert(10, "bar");
	tree.insert(12, "twelve");

	std::vector exp = {3,5,10,12};
	std::vector<int> trav = tree.inOrder();
	EXPECT_TRUE(trav == exp);
}

TEST(MyTests, PreOrder1)
{
	MyAVLTree<int,std::string> tree;
	tree.insert(5, "foo");
	tree.insert(3, "sna");
	tree.insert(10, "bar");
	tree.insert(12, "twelve");

	std::vector exp = {5,3,10,12};
	std::vector<int> trav = tree.preOrder();
	EXPECT_TRUE(trav == exp);
}

TEST(MyTests, PostOrder1)
{
	MyAVLTree<int,std::string> tree;
	tree.insert(5, "foo");
	tree.insert(3, "sna");
	tree.insert(10, "bar");
	tree.insert(12, "twelve");

	std::vector exp = {3,12,10,5};
	std::vector<int> trav = tree.postOrder();
	EXPECT_TRUE(trav == exp);
}

TEST(MyTests, PostOrder2RebalancingDoneThistimeTho)
{
	MyAVLTree<int,std::string> tree;
	tree.insert(1,"Boo");
	tree.insert(2,"Alex");
	tree.insert(3,"Eric");
	tree.insert(4,"Kenny");
	tree.insert(5,"Kyle");


	std::vector exp = {1,3,5,4,2};
	std::vector<int> trav = tree.postOrder();
	EXPECT_TRUE(trav == exp);
}

TEST(MyTests, InOrder2RebalancingDoneThistimeTho)
{
	MyAVLTree<int,std::string> tree;
	tree.insert(1,"Boo");
	tree.insert(2,"Alex");
	tree.insert(3,"Eric");
	tree.insert(4,"Kenny");
	tree.insert(5,"Kyle");


	std::vector exp = {1,2,3,4,5};
	std::vector<int> trav = tree.inOrder();
	EXPECT_TRUE(trav == exp);
}

TEST(MyTests, PreOrder2RebalancingDoneThistimeTho)
{
	MyAVLTree<int,std::string> tree;
	tree.insert(1,"Boo");
	tree.insert(2,"Alex");
	tree.insert(3,"Eric");
	tree.insert(4,"Kenny");
	tree.insert(5,"Kyle");


	std::vector exp = {2,1,4,3,5};
	std::vector<int> trav = tree.preOrder();
	EXPECT_TRUE(trav == exp);
}


TEST(PostCheckPoint, InOrderTraversal)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(5, "foo");
	tree.insert(3, "sna");
	tree.insert(10, "bar");
	tree.insert(12, "twelve");
	tree.insert(15, "fifteen");

	std::vector<int> trav = tree.inOrder();
	std::vector<int> expected = {3, 5, 10, 12, 15};
	EXPECT_TRUE( trav == expected );
}

TEST(MyTest, InOrderTraversal1)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(20, "Boo");
	tree.insert(12, "Eric");
	tree.insert(13, "Eric");
	

	std::vector<int> trav = tree.inOrder();
	std::vector<int> expected = {12,13,20};
	EXPECT_TRUE( trav == expected );
}

TEST(MyTest, InOrderTraversal2)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(20, "Boo");
	tree.insert(12, "Eric");
	tree.insert(13, "Stan");
	tree.insert(21, "Kenny");
	tree.insert(22, "Kyle");
	

	std::vector<int> trav = tree.inOrder();
	std::vector<int> expected = {12,13,20,21,22};
	EXPECT_TRUE( trav == expected );
}

TEST(MyTest, InOrderTraversal3)
{
	MyAVLTree<int, std::string> tree;
	tree.insert(20, "Boo");
	tree.insert(12, "Eric");
	tree.insert(13, "Stan");
	tree.insert(21, "Kenny");
	tree.insert(22, "Kyle");
	tree.insert(23, "Butters");
	

	std::vector<int> trav = tree.inOrder();
	std::vector<int> expected = {12,13,20,21,22,23};
	EXPECT_TRUE( trav == expected );
}

TEST(PostCheckPoint, JackSparrow)
{
	std::string quote = "I'm dishonest, and a dishonest man you can ";
	quote += "always trust to be dishonest. Honestly. It's the honest ";
	quote += "ones you want to watch out for, because you can never ";
	quote += "predict when they're going to do something incredibly... stupid.";


	std::istringstream stream( quote );

	MyAVLTree<std::string, unsigned> tree;

	countWords(stream, tree);
	EXPECT_TRUE(tree.find("dishonest") == 3);
}

TEST(PostCheckPoint, JackSparrowcopyorNAH)
{
	std::string quote = "I'm dishonest, and a dishonest man you can ";
	quote += "always trust to be dishonest. Honestly. It's the honest ";
	quote += "ones you want to watch out for, because you can never ";
	quote += "predict when they're going to do something incredibly... stupid.";


	std::istringstream stream( quote );

	MyAVLTree<std::string, unsigned> tree;

	countWords(stream, tree);
	EXPECT_TRUE(tree.find("dishonest") == 3);
}



TEST(MyTest, CommunistManifesto)
{

	std::ifstream f("communist.txt");
	std::string quote;
	if (f)
	{
		std::ostringstream ss;
		ss << f.rdbuf();
		quote = ss.str();
	}

	std::istringstream stream( quote );

	MyAVLTree<std::string, unsigned> tree;

	countWords(stream, tree);
	EXPECT_TRUE(tree.find("proletariat") == 63);
}


// TEST(MyTest, Tolstoy)
// {

// 	std::ifstream f("war.txt");
// 	std::string quote;
// 	if (f)
// 	{
// 		std::ostringstream ss;
// 		ss << f.rdbuf();
// 		quote = ss.str();
// 	}

// 	std::istringstream stream( quote );

// 	MyAVLTree<std::string, unsigned> tree;

// 	countWords(stream, tree);
// 	EXPECT_TRUE(tree.find("Vienna") == 49);
// }


// TEST(MyTest, Hamlet)
// {

// 	std::ifstream f("ham.txt");
// 	std::string quote;
// 	if (f)
// 	{
// 		std::ostringstream ss;
// 		ss << f.rdbuf();
// 		quote = ss.str();
// 	}

// 	std::istringstream stream( quote );

// 	MyAVLTree<std::string, unsigned> tree;

// 	countWords(stream, tree);
// 	EXPECT_TRUE(tree.find("was") == 60);
// }


}